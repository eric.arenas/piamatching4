<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/usuari', function () {
    return view('usuaris/usuaris');
});

Route::get('/', function () {
    return view('login');
});

Route::get('/preguntes', function () {
    return view('preguntes/preguntes');
});

Route::resource('usuaris','UsuariController');
Route::resource('preguntas','PreguntaController');
Route::resource('matchs','MatchController');
Route::resource('resultats','ResultatController');
Route::resource('administradors','AdministradorController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});


Route::post('/admin', function () { 
    $username = $_POST['user'];
    $pass = $_POST['pass'];
    $admin = DB::select('select * from administradors WHERE username = ? AND password = ?', [$username, $pass]);

    if(!empty($admin)){
        Session::put('admin', true);
        return view('/dashboard');
    } else { 
        return view('/');
    }
});

// Route::post('/crear', function () { 
//     $pregunta = $_POST['pregunta'];
//     $puntuacion = $_POST['puntuacion'];
//     echo "hola";
//     DB::table('preguntas')->insert(['pregunta'=>$pregunta ,'puntuatge'=>$puntuacion]);
//     return view('preguntes/preguntes');
    
// });

Route::post('/crear', 'PreguntaController@create');
Route::post('/crearUsuari', 'UsuariController@create');
Route::post('/edit/{id}', 'PreguntaController@update');
Route::get('/delete/{id}', 'PreguntaController@destroy');
Route::post('/editUsuari/{id}', 'UsuariController@update');
Route::get('/deleteUsuari/{id}', 'UsuariController@destroy');
