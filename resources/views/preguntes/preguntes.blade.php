@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

<head>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>

    <body>
    <?php
    $array = DB::select('select * from preguntas');
    
    ?>
    <style>
    body{
        background-color: 		#FCB07E	!important;
    }
    .rosica{
        background-color: 		#FCB07E	!important;
    }
    </style>

 <button type="button" data-toggle="modal" data-target="#exampleModalLong"  style="  position: relative; left: 90%;" class="btn btn-primary m-4">Crear</button>


    <table id="table">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Puntuacio</th>
            <th>Data</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($array as $row)
        <tr>
            <td>{{$row->pregunta}}</td>
            <td>{{$row->puntuatge}}</td>
            <td>{{$row->created_at}}</td>
            <td>
            <button type="button" data-toggle="modal" data-target="#editar"  class="btn btn-primary" >editar</button>
 
            <button type="button" data-toggle="modal" data-target="#eliminar"  class="btn btn-primary">eliminar</button>
            </td>
        </tr>
        @endforeach
    </tbody>
    </table>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
    $(document).ready(function(){
    $('#table').DataTable();
    });
    </script>
    </body>
@endpush

<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pregunta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

    <form  method="POST" action="/crear">
    @csrf
    <div class="form-group">
        <label for="textpregunta">Pregunta</label>
        <input type="text" class="form-control"name="pregunta" id="preguntainput" placeholder="Aquí pregunta">
        <input type="text" class="form-control" name="puntuacion" id="puntuacioninput" placeholder="Aquí puntuación">
    </div>
    <button type="submit" id="crear" class="btn btn-primary">Enviar</button>
    </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="editarTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editarTitle">Pregunta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

    <form  method="post" action="/edit/{{$row->id}}">
    @csrf
    <div class="form-group">
        <label for="textpregunta">Pregunta</label>
        <input type="text" class="form-control"name="id" id="id" value="{{$row->id}}" hidden>
        <input type="text" class="form-control"name="pregunta" id="preguntainput" value="{{$row->pregunta}}">
        <input type="text" class="form-control" name="puntuacion" id="puntuacioninput" value="{{$row->puntuatge}}">
    </div>
    <button type="submit" id="crear" class="btn btn-primary">Enviar</button>
    </form>
    </div>
    </div>
  </div>
</div>


<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="eliminarTitle">Pregunta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

    <form  method="get" action="/delete/{{$row->id}}">
    @csrf
    <div class="form-group">
        <label for="textpregunta">Pregunta</label>
        <input type="text" class="form-control"name="id" id="id" value="{{$row->id}}" >
        <input type="text" class="form-control"name="pregunta" id="preguntainput" value="{{$row->pregunta}}">
        <input type="text" class="form-control" name="puntuacion" id="puntuacioninput" value="{{$row->puntuatge}}">
    </div>
    <button type="submit" id="crear" class="btn btn-primary">Enviar</button>
    </form>
    </div>
    </div>
  </div>
</div>