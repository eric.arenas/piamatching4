@extends('master.master')
@section('content')
<div class="container-fluid h-100">
    <div class="row h-100">
        <div class="col-mx-6 w-50" style="background-color:#ffeee6;">
        <img src="/img/logo.jpg" class="w-70" style=" margin-top: 5%;margin-left: 10%;margin-right: 10%; display: block;"   alt="">
        </div>
        <div class="col-mx-6 w-50 d-flex justify-content-center" style="background-color:black">
        <form class="w-50 d-flex justify-content-center flex-column" style="background: #ffeee6;padding: 5%; margin-top: 18%; margin-bottom: 25%" method="POST" action="/admin">
        {{ csrf_field() }}
            <div class="form-group">
            <p class="text-center" style="font-size: 40px;">Login</p>
                <label for="usuari">Usuari</label>
                <input type="text" class="form-control" name="user" id="usuari" aria-describedby="emailHelp" placeholder="Usuari">
            </div>
            <div class="form-group">
                <label for="contrasenya">Contrasenya</label>
                <input type="password" class="form-control" name="pass" id="contrasenya" placeholder="Contrasenya">
            </div>
            <button type="submit" class="btn btn-primary">Go</button>
        </form>
        </div>
    </div>
</div>
@stop