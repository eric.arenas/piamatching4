<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuaris', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('cognom');
            $table->string('usuari');
            $table->string('password');
            $table->string('curs');
            $table->longtext('descripcio');
            $table->string('foto')->default('foto_perfil.png');
            $table->date('data_neixement');
            $table->string('email');
            $table->char('sexe');
            $table->integer('telefono');
            $table->interger('bloquejat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuaris');
    }
}
