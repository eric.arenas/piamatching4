<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    //
    protected $fillable = ['percentatge','amistat'];

    public function usuari(){
        return $this->belongsTo('App\Usuari');
    }
    public function resultat(){
        return $this->hasMany('App\resultat');
    }
}
