<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    //
    protected $fillable = ['pregunta','puntuatge'];


    public function resultat(){
        return $this->hasMany('App\Resultat');
    }
   
}
