<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuari extends Model
{
    //
    protected $fillable = ['nom','usuari','cognom','password','curs','descripcio','foto','data_neixement','email','sexe','telefono','bloquejat'];

    public function match(){
        return $this->hasMany('App\Match');
    }
    public function resultat(){
        return $this->belongsTo('App\Resultat');
    }


}
