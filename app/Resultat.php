<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultat extends Model
{
    //
    protected $fillable = ['respostes'];

    public function usuari(){
        return $this->belongsTo('App\Usuari');
    }
    public function pregunta(){
        return $this->belongsTo('App\Pregunta');
    }
}
